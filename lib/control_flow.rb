# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  letters = str.chars
  letters.map! { |l| letters.delete(l) if l == l.upcase }
  letters.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  return str[str.length / 2] if str.length.odd?
  return str[(str.length / 2 - 1)..(str.length / 2)] if str.length.even?
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.select do |l|
    l if VOWELS.include?(l)
  end.length
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).to_a.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined = ""
  arr.each do |l|
    joined << separator << l if joined.size > 0
    joined << l if joined.empty?
  end
  joined
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  arr = str.downcase.chars
  arr.map.with_index { |l, i| i.odd? ? l.capitalize : l }.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split
  words.map { |word| word.length >= 5 ? word.reverse : word }.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).to_a.map do |num|
    if num % 15 == 0 then "fizzbuzz"
    elsif num % 3 == 0 then "fizz"
    elsif num % 5 == 0 then "buzz"
    else num
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []
  idx = arr.length
  until idx == 0
    reversed << arr[idx-1]
    idx -= 1
  end
  reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  (2..num).select { |n| num % n == 0 }.size == 1
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select { |n| num % n == 0 }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).map { |n| n if prime?(n) }.compact
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).size
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = []
  even = []
  arr.each { |n| n.even? ? even << n : odd << n }
  even.length == 1 ? even[0] : odd[0]
end
